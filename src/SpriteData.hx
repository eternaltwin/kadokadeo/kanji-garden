import haxe.ds.StringMap;

class SpriteData {
	static public var data = [
		"bamboo" => [75, 448],
		"banana" => [117, 103],
		"bonus" => [339, 153],
		"bottom" => [144, 152],
		"bounce" => [222, 113],
		"feuilles" => [319, 107],
		"fg" => [458, 347],
		"herbes" => [398, 102],
		"hero" => [309, 220],
		"hero_wait" => [524, 373],
		"kunai" => [84, 60],
		"mcBg" => [921, 301],
		"monkey_1" => [350, 251],
		"monkey_2" => [350, 251],
		"monkey_3" => [349, 251],
		"part" => [23, 36],
		"score" => [195, 359],
		"warning" => [85, 29],
		"taupe" => [566, 391],
		"bamboo_bottom" => [143, 151],
		"kunai_bounce" => [192, 42],
	];
	static public var anchor:StringMap<Array<Int>> = new StringMap();
}
